import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResultPage } from './result.page';

const routes: Routes = [
  {
    path: '',
    component: ResultPage
  },
  {
    path: 'result/:duration',
    loadChildren: () => import('./result.module').then( m => m.ResultPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResultPageRoutingModule {}