import { ThrowStmt } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { NgModel } from '@angular/forms';
import { GameQuestion } from '../gamequestion';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  private questions: GameQuestion[] = [];
  private activeQuestion: GameQuestion;
  private isCorrect: boolean;
  private isSelected: boolean;
  private questionCounter: number;
  private startTime: Date;
  private endTime: Date;
  private duration: number;
  public score: number;


  constructor() { }

  public addQuestions() {
    this.questions = [{
      quote: "Lara Croft",
      options: [
        "Tomb Raider",
        "Uncharted",
        "The Last Of Us"],
      correctOption: 0
    },
    {
      quote: "Roadhog",
      options: [
        "Team Fortress 2",
        "Tom Clancy's Rainbow Six: Siege",
        "Overwatch"],
      correctOption: 2
    },
    {
      quote: "Link",
      options: [
        "Super Mario",
        "Pokémon",
        "Zelda"],
      correctOption: 2
    },
    {
      quote: "Agent 47",
      options: [
        "Metal Gear Solid",
        "Hitman",
        "Splinter Cell"],
      correctOption: 1
    },
    {
      quote: "Geralt of Rivia",
      options: [
        "The Witcher ",
        "Skyrim",
        "Borderlands"],
      correctOption: 0
    }
    ];
  }

  public setQuestion() {
    this.isCorrect = false;
    this.isSelected = false;
    this.activeQuestion = this.questions[this.questionCounter];
    this.questionCounter++; 
  }

  public initialize() {
    this.questionCounter = 0;
    this.score = 0;
    this.startTime = new Date();
    this.setQuestion();
  }

  public getQuestions(): GameQuestion[] {
    return this.questions;
  }

  public getActiveQuestion(): GameQuestion {
    return this.activeQuestion;
  }

  public getQuoteOfActiveQuestion(): string {
    return this.activeQuestion.quote;
  }

  public getOptionsOfActiveQuestion(): string[] {
    return this.activeQuestion.options;
  }

  public getIndexOfActiveQuestion(): number {
    return this.questionCounter;
  }

  public getNumberOfQuestions(): number {
    return this.questions.length;
  }

  public getCorrectOptionOfActiveQuestion(): string {
    return this.activeQuestion.options[this.activeQuestion.correctOption];
  }

  public setOptionSelected() {
    this.isSelected = true;
  }

  public isOptionSelected(): boolean {
    return this.isSelected;
  }


  public isCorrectOption(option: number): boolean {
    this.isCorrect = (option === this.activeQuestion.correctOption) ? true : false;
    return this.isCorrect;
  }

  public isAnswerCorrect(): boolean {
    return this.isCorrect;
  }

  public calculateScore() {
    if (this.isAnswerCorrect() === true ) {
      this.score++;
    } else {
      this.score = 0;
    }
  }

  public isFinished(): boolean {
    return (this.questionCounter === this.questions.length) ? true : false;
  }

  public getDuration(): number {
    this.endTime = new Date();
    this.duration = this.endTime.getTime() - this.startTime.getTime();
    return this.duration;
  }

}
