import { Component } from '@angular/core';
import { QuizService } from '../quiz.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(public router: Router, private quizService: QuizService) {}

  private feedback: string;
  private duration: number;
  private correctCounter: number;

  ngOnInit() {
    this.quizService.addQuestions();
    this.quizService.initialize();
    this.feedback = "";
  }


  private checkOption(option: number) {
    this.quizService.setOptionSelected();
      if (this.quizService.isCorrectOption(option)) {
        this.quizService.score++
        this.feedback =
        this.quizService.getCorrectOptionOfActiveQuestion() +
        ' is correct! Correct answers: ' + this.quizService.score + '/5';
      }
      else {
        this.continue();
      }
  }

  private continue() {
    if (this.quizService.isFinished()) {
      this.duration = this.quizService.getDuration();
      this.router.navigateByUrl('result/' + this.duration);
    }
    else {
      this.quizService.setQuestion();
    }
  }
}
