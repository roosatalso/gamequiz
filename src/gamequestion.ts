export class GameQuestion {
    quote: string;
    options: string[];
    correctOption: number;
}